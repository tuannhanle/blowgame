﻿using System.Collections;
using System.Collections.Generic;

public interface IMovement
{
    void Move(float mVertical, float mHorizontal, float mSpeed);
}
public interface IEating
{
    void Eat();
    void BeEaten();
    bool IsPrey(float mValue);
}
