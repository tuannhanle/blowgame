﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eating : MonoBehaviour, IEating
{
    [SerializeField]
    private Entity _entity;
    void Awake()
    {

        if (_entity == null)
        {
            _entity = GetComponent<Entity>();
        }
    }
    public bool IsPrey(float mValue)
    {
        if (_entity.Value<mValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Eat()
    {
        _entity.Value+=0.1f;
        _entity.Render();
    }

    public void BeEaten()
    {
        _entity.Value-=0.1f;
        _entity.Render();
        if (_entity.Value<=0)
        {
            this.gameObject.SetActive(false);
        }
    }

}
