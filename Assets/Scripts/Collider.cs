﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collider : MonoBehaviour
{
    [SerializeField]
    private Entity _entity;
    private void Awake()
    {
        if (_entity == null)
        {
            _entity = GetComponent<Entity>();
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        var mOpponent = collision.gameObject.GetComponent<IEating>();

        if (mOpponent!=null)
        {
            if (mOpponent.IsPrey(_entity.Value))
            {
                GetComponent<IEating>().Eat();
                mOpponent.BeEaten();
            }
            else
            {
                mOpponent.Eat();
                GetComponent<IEating>().BeEaten();
            }
        }
    }
    
}
