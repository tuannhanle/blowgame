﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region lazy singleton
    private static PlayerController _instance;
    private PlayerController() { }
    public static PlayerController getInstance()
    {
        if (_instance == null)
        {
            _instance = new PlayerController();
        }
        return _instance;
    }
    #endregion

    [SerializeField]
    public Player player;
    [SerializeField]
    public Movement playerMovement;
    private void Awake()
    {
        CheckNullInstance();
    }

    private void CheckNullInstance()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if (player == null)
        {
            player = this.gameObject.GetComponent<Player>();
        }
        if (playerMovement == null)
        {
            playerMovement = this.gameObject.GetComponent<Movement>();
        }
    }

    public void IdleState()
    {
        player.AwakePlayer();
        player.Render();
    }
    public void PlayingState()
    {
        playerMovement.enabled = true;
    }
}
