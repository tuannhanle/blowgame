﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region lazy singleton
    private static GameManager _instance;
    private GameManager() { }
    public static GameManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new GameManager();
        }
        return _instance;
    }
    #endregion
    public enum StateType { Idle, Playing, Pause, Lose, Win}
    private StateType _state;

    public StateType State
    {
        get { return _state; }
        set { _state = value; }
    }
        

    // Start is called before the first frame update
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        State = StateType.Idle;

    }
    private void Update()
    {
        switch (State)  
        {
            case StateType.Idle:
                IdleState();
                break;
            case StateType.Playing:
                PlayingState();
                break;
            case StateType.Pause:
                break;
            case StateType.Lose:
                break;
            case StateType.Win:
                break;
            default:
                break;
        }
    }

    private void PlayingState()
    {
        PlayerController.getInstance().PlayingState();
        //PlayerController

    }

    private void IdleState()
    {
        PlayerController.getInstance().IdleState();
        //throw new NotImplementedException();
        State = StateType.Playing;
    }
}
