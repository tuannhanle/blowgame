﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour, IMovement
{
    [SerializeField]
    private Entity _entity;

    [SerializeField]
    private Rigidbody2D _rb;



    // Start is called before the first frame update
    void Awake()
    {
        if (_rb==null)
        {
            _rb = GetComponent<Rigidbody2D>();
        }
        if (_entity==null)
        {
            _entity = GetComponent<Entity>();
        }
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        Move(_entity.Vertical, _entity.Horizontal, _entity.Speed);
    }
    public void Move(float mVertical, float mHorizontal, float mSpeed)
    {
        Vector3 mDirection = Vector3.up * mVertical + Vector3.right * mHorizontal;
        _rb.AddForce(mDirection * mSpeed * Time.fixedDeltaTime);
    }
}
