﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject _virusSample;
    [SerializeField]
    private GameObject _enityZone;
    [SerializeField]
    private List<GameObject> _virusList;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            GameObject mVirus = Instantiate(_virusSample, _enityZone.transform);
            Virus mVirusComponent = mVirus.GetComponent<Virus>();
            mVirusComponent.Value = 5;
            mVirusComponent.Speed = 50;
            mVirusComponent.AwakePosition(new Vector3(
                Random.Range(-2.5f, 2.5f),
                Random.Range(-5f, 5f),
                0
                ));
            _virusList.Add(mVirus);
        }
        foreach (GameObject mVirus in _virusList)
        {
            mVirus.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
