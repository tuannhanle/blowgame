﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    #region Private variable
    private float _value=20;
    private float _speed=20;
    private float _horizontal=0;
    private float _vertical=0;
    #endregion

    #region Public properties   
    public float Value
    {
        get { return _value; }
        set { _value = value; }
    }
    public float Vertical
    {
        get { return _vertical; }
        set { _vertical = value; }
    }

    public float Horizontal
    {
        get { return _horizontal; }
        set { _horizontal = value; }
    }
    public float Speed
    {
        get { return _speed; }
        set { _speed = value; }
    }
    #endregion  



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Render()
    {

    }
    public void AwakePosition(Vector3 vector3)
    {

    }
}
