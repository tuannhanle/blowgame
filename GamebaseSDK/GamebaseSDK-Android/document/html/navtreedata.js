var NAVTREE =
[
  [ "Gamebase SDK for Android", "index.html", [
    [ "Deprecated APIs", "md__deprecated_a_p_is.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_auth_gamebase_token_8java.html",
"classcom_1_1toast_1_1android_1_1gamebase_1_1_gamebase_web_view_configuration_1_1_builder.html#abbad620c8f992e84f5c4c64b98b65798",
"classcom_1_1toast_1_1android_1_1gamebase_1_1base_1_1_network_manager.html#acdbf2cda592e7b95d250246e4eab7cee",
"dir_c1a7a7a0af1039e3a9829b55953e8856.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';